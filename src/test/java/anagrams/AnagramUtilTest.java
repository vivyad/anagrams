package anagrams;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class AnagramUtilTest {


	private static final Set<String> SET_1 = new TreeSet<>(Arrays.asList("act", "cat"));
	private static final Set<String> SET_2 = new TreeSet<>(Arrays.asList("acre", "care", "race"));

	/**
	 * Test method for accessing the constructor of {@link AnagramUtil}.
	 * 
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 */
	@Test(expected = InvocationTargetException.class)
	public void testAnagramUtil()
			throws NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
		// Accessing constructor illegally through reflection
		// Accessing constructor illegally through reflection
		final Constructor<AnagramUtil> constructor = AnagramUtil.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		constructor.newInstance();
	}

	/**
	 * Test method for {@link AnagramUtil#getPrimeProduct(String)} with value.
	 */
	@Test
	public void testGetPrimeProductString() {
		final long primeProductAAA = AnagramUtil.getPrimeProduct("aaa");
		assertEquals("The prime product of 'aaa' must be 8.", 8, primeProductAAA);
		final long primeProductBBB = AnagramUtil.getPrimeProduct("bbb");
		assertEquals("The prime product of 'bbb' must be 27.", 27, primeProductBBB);
	}

	/**
	 * Test method for {@link AnagramUtil#getPrimeProduct(String)} with single
	 * character.
	 */
	@Test
	public void testGetPrimeProductStringSingleCharacter() {
		final long primeProductA = AnagramUtil.getPrimeProduct("a");
		assertEquals("The prime product of any single character must be 1.", 1, primeProductA);
		final long primeProductB = AnagramUtil.getPrimeProduct("b");
		assertEquals("The prime product of any single character must be 1.", 1, primeProductB);
	}

	/**
	 * Test method for {@link AnagramUtil#getPrimeProduct(String)} with single
	 * character.
	 */
	@Test
	public void testGetPrimeProductStringNullAndBlank() {
		final long primeProductNull = AnagramUtil.getPrimeProduct(null);
		assertEquals("The prime product of a 'null' input must be 1.", 1, primeProductNull);
		final long primeProductBlank = AnagramUtil.getPrimeProduct("  ");
		assertEquals("The prime product of a blank text must be 1.", 1, primeProductBlank);
	}

	/**
	 * Test method for {@link AnagramUtil#getPrimeProduct(String)} with special
	 * character.
	 */
	@Test
	public void testGetPrimeProductStringSpecialCharacter() {
		final long primeProductNull = AnagramUtil.getPrimeProduct("af3");
		assertEquals("The prime product of a 'null' input must be 1.", 1, primeProductNull);
		final long primeProductBlank = AnagramUtil.getPrimeProduct("af}");
		assertEquals("The prime product of a blank text must be 1.", 1, primeProductBlank);
	}

	/**
	 * Test method for {@link AnagramUtil#PRIME_NUMBER_FOR_ALPHABET} variable.
	 */
	@Test
	public void testPrimeNumberForAlphabet() {
		List<Integer> expectedList = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 
				47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101);
		assertEquals("The Prime number list should be equivalend to the expected list.", expectedList,
				AnagramUtil.PRIME_NUMBER_FOR_ALPHABET);
	}

	/**
	 * Test method for {@link AnagramUtil#PRIME_NUMBER_FOR_ALPHABET} variable to
	 * validate the non-modification of the list variable;
	 */
	@Test
	public void testPrimeNumberForAlphabetNonModification() {
		String expectedCause = "java.lang.UnsupportedOperationException";
		try {
			AnagramUtil.PRIME_NUMBER_FOR_ALPHABET.add(1);
		} catch (UnsupportedOperationException ex) {
			assertEquals("Exeception caused by, should match the exepected cause.", expectedCause, ex.toString());
		}
	}


	/**
	 * Test {@link AnagramUtil#getAnagramList(String)} with valid anagrams.
	 */
	@Test
	public void testProcessAnagramsString() {
		final List<Set<String>> expectedList = Arrays.asList(SET_1, SET_2);
		// Get anagram list
		final List<Set<String>> list = AnagramUtil.getAnagramList(AnagramUtil.INPUT);
		// Assertion Begins
		assertEquals("List must be equivalent to the expected list.", expectedList, list);
	}

	/**
	 * Test {@link AnagramUtil#getAnagramList(String)} with input file.
	 * Input file 'input.txt' has -> 5 anagram words and 2 anagrams.
	 */
	@Test
	public void testProcessAnagramsStringInputFile() throws IOException {
		final List<Set<String>> expectedList = Arrays.asList(SET_1, SET_2);
		// Scan File for Test
		final String inputFile = "input.txt";
		String words;
		try (final InputStream stream = AnagramUtilTest.class.getClassLoader().getResourceAsStream(inputFile)) {
			words = IOUtils.toString(stream, StandardCharsets.UTF_8).trim();
		}
		// Get anagram list
		final List<Set<String>> list = AnagramUtil.getAnagramList(words);
		// Assertion Begins
		assertEquals("List must be equivalent to the expected list.", expectedList, list);
	}

	/**
	 * Test {@link AnagramUtil#getAnagramList(String)} with input file with lot of
	 * anagram. Input file 'input_big.txt' has -> 241 anagrams.
	 */
	@Test
	public void testProcessAnagramsStringInputFile2() throws IOException {
		// Content  within the file "input_big.txt" is referred from web-site:
		// http://www.english-for-students.com/Complete-List-of-Anagrams.html
		final String inputFile2 = "input_big.txt";
		String words;
		try (final InputStream stream = AnagramUtilTest.class.getClassLoader().getResourceAsStream(inputFile2)) {
			words = IOUtils.toString(stream, StandardCharsets.UTF_8); // .trim() // Commented purposely
		}
		// Get anagram list
		final List<Set<String>> list = AnagramUtil.getAnagramList(words);
		// Assertion Begins
		assertNotNull("Should be not null.", list);
		assertEquals("List size must be equivalent to the expected size.", 241, list.size());
	}

	/**
	 * Test sorting of {@link AnagramUtil#getAnagramList(String)} with
	 * valid anagrams.
	 */
	@Test
	public void testProcessAnagramsStringChangeOrder() {
		// Changed the order: [0]->1 and [1]->0
		final List<Set<String>> unexpectedList = Arrays.asList(SET_2, SET_1);
		// Get anagram list
		final List<Set<String>> list = AnagramUtil.getAnagramList(AnagramUtil.INPUT);
		// Assertion Begins
		assertNotNull("Should be not null.", list);
		assertNotEquals("List must be not equivalent to the unexpected list, due to change in order.", unexpectedList,
				list);
		assertEquals("Objects from the lists should be equal", list.get(0), unexpectedList.get(1));
		assertEquals("Objects from the lists should be equal", list.get(1), unexpectedList.get(0));
	}

	/**
	 * Test {@link AnagramUtil#getAnagramList(String)} with input having single
	 * character.
	 */
	@Test
	public void testProcessAnagramsStringNoAnagrams() {
		String message = "The list is expected to be empty due no anagrams.";
		assertTrue(message, AnagramUtil.getAnagramList("aba\nabc").isEmpty());
		assertTrue(message, AnagramUtil.getAnagramList("run\nurnn").isEmpty());
	}

	/**
	 * Test {@link AnagramUtil#getAnagramList(String)} with invalid inputs.
	 * <ul>
	 * <li><code>null</code></li>
	 * <li>""</li>
	 * <li>numbers</li>
	 * </ul>
	 */
	@Test
	public void testProcessAnagramsStringInvalidInput() {
		String message = "The list is expected to be empty due to invalid input words.";
		assertTrue(message, AnagramUtil.getAnagramList("12\n12").isEmpty());
		assertTrue(message, AnagramUtil.getAnagramList("12\n\n").isEmpty());
		assertTrue(message, AnagramUtil.getAnagramList("a\na").isEmpty());
	}

	/**
	 * Test {@link AnagramUtil#getAnagramList(String)} with input having single
	 * character.
	 */
	@Test
	public void testProcessAnagramsStringNullAndBlank() {
		assertTrue("The list is expected to be empty.", AnagramUtil.getAnagramList((String) null).isEmpty());
		assertTrue("The list is expected to be empty.", AnagramUtil.getAnagramList("").isEmpty());
	}

}
