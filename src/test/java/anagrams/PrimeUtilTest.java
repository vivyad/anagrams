package anagrams;

import static org.junit.Assert.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

/**
 * Tests for {@link PrimeUtil}
 */
public class PrimeUtilTest {

	/**
	 * Test method for accessing the constructor of {@link PrimeUtil}.
	 * 
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	@Test
	public void testPrimeUtil() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException {
		final String expectedCause = "java.lang.reflect.InvocationTargetException";
		final Constructor<PrimeUtil> constructor = PrimeUtil.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		try {
			constructor.newInstance();
		} catch (InvocationTargetException ex) {
			assertEquals("Exeception caused by, should match the exepected cause.", expectedCause, ex.toString());
		}
	}

	/**
	 * Testing with the whole number as input value.
	 */
	@Test
	public void testOrderedPrimeNumberList() {
		// Predefining the prime numbers in list for testing purpose
		final List<Integer> expectedList = Arrays.asList(2, 3, 5, 7, 11, 13, 17, 19, 23, 29);
		final int count = expectedList.size();
		final List<Integer> list = PrimeUtil.orderedPrimeNumberList((short) count);
		assertEquals("Count should match the list size.", count, list.size());
		assertEquals("List should match with the expected list.", expectedList, list);
	}

	/**
	 * Testing with zero as an input value
	 */
	@Test
	public void testOrderedPrimeNumberListWithCountZero() {
		final List<Integer> list = PrimeUtil.orderedPrimeNumberList((short) 0);
		assertEquals("Size of list should be 0.", 0, list.size());
	}

	/**
	 * Testing with negative integer as an input value
	 */
	@Test
	public void testOrderedPrimeNumberListWithCountMinusOne() {
		final List<Integer> list = PrimeUtil.orderedPrimeNumberList((short) -1);
		assertEquals("Size of list is expected to be 0.", 0, list.size());
	}

	/**
	 * Testing with {@link Short#MAX_VALUE} as an input value. If
	 * {@link Integer#MAX_VALUE} is considered then the VM runs out of memory.
	 */
	@Test
	public void testOrderedPrimeNumberListMaxCount() {
		final List<Integer> list = PrimeUtil.orderedPrimeNumberList(Short.MAX_VALUE);
		assertEquals("Size of list should be Short.MAX_VALUE.", Short.MAX_VALUE, list.size());
	}

}
