package anagrams;

import static org.junit.Assert.*;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

public class AnagramAppTest {

	/**
	 * Test method for accessing the constructor of {@link PrimeUtil}.
	 * 
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 */
	@Test
	public void testPrimeUtil() throws NoSuchMethodException, SecurityException, InstantiationException,
			IllegalAccessException, IllegalArgumentException {
		final String expectedCause = "java.lang.reflect.InvocationTargetException";
		final Constructor<AnagramApp> constructor = AnagramApp.class.getDeclaredConstructor();
		constructor.setAccessible(true);
		try {
			constructor.newInstance();
		} catch (InvocationTargetException ex) {
			assertEquals("Exeception caused by, should match the exepected cause.", expectedCause, ex.toString());
		}
	}

	/**
	 * Test {@link AnagramUtil#main} method using <code>null</code>, blank and
	 * contents from the file.
	 */
	@Test
	public void testMain() throws IOException {
		AnagramApp.main(null);
		AnagramApp.main(new String[] {});
		AnagramApp.main(new String[] { "" });
		AnagramApp.main(new String[] { null });
		try (final InputStream stream = AnagramUtilTest.class.getClassLoader().getResourceAsStream("input.txt")) {
			AnagramApp.main(new String[] { IOUtils.toString(stream, StandardCharsets.UTF_8) });
		}
		assertTrue("If this code is executed, then test is successful.", true);
	}

	/**
	 * Test {@link AnagramUtil#main} method using <code>null</code>, blank and
	 * contents from the file.
	 */
	@Test
	public void testMainBig() throws IOException {
		try (final InputStream stream = AnagramUtilTest.class.getClassLoader().getResourceAsStream("input_big.txt")) {
			final String printInput = "false";
			AnagramApp.main(new String[] { IOUtils.toString(stream, StandardCharsets.UTF_8), printInput });
		}
		assertTrue("If this code is executed, then test is successful.", true);
	}

	/**
	 * Test {@link AnagramUtil#main(String)} method using file with '33967' words.
	 * 
	 * @throws IOException
	 */
	@Test
	public void testMainMax() throws IOException {
		// Content within the file "input_biggest.txt" is referred from web-site:
		// http://www.english-for-students.com/Complete-List-of-Anagrams.html
		try (final InputStream stream = AnagramUtilTest.class.getClassLoader().getResourceAsStream("input_biggest.txt")) {
		    // Avoiding to print the input, due to its large size
			final String printInput = "false";
			AnagramApp.main(new String[] { IOUtils.toString(stream, StandardCharsets.UTF_8), printInput });
		}
		assertTrue("If this code is executed, then test is successful.", true);
	}

}
