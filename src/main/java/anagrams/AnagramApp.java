package anagrams;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Anagram application through {@link #main(String[])}.
 * 
 * @author Vivek
 * 
 * @created 10 Aug 2020
 *
 */
public class AnagramApp {

	private static final Logger LOG = LoggerFactory.getLogger(AnagramApp.class);

	private AnagramApp() throws InvocationTargetException {
		// Utility classes, which are collections of static members, are not meant to be
		// instantiated.
		throw new InvocationTargetException(new IllegalAccessException("Utility class"));
	}

	/**
	 * This main method accepts the {@link String[]} <code>args</code>, where the
	 * first element in the array should contain the input string. If the valid input
	 * is not defined at the first index / position within the array then default value
	 * is used: {@link #INPUT}. Same condition applies when the array is null or empty.
	 * <br/>
	 * If the {@link String} array instance i.e. <code>args</code> contains the value
	 * 'false' at the second index / position within <code>args</code>, the respective
	 * input will not be printed in the output console or the log file.
	 * 
	 * @param args {@link String}
	 *             Array containing the input at the first position.
	 */
	public static void main(final String[] args) {

		// Get input
		final boolean hasInput = args != null && args.length > 0 && args[0] != null && !args[0].equals("");
		final String input = hasInput ? args[0] : AnagramUtil.INPUT;
		// Process input to discover anagrams from the input words
		final List<Set<String>> anagramSetList = AnagramUtil.getAnagramList(input);
		// Print input conditionally
		final boolean toPrintInput = !hasInput || args.length < 2 || !"false".equals(args[1]);
		if (toPrintInput) {
			LOG.info("Input Content:\n{}\n", input);
		}

		// Log the words
		int i = 0;
		final StringBuilder sb = new StringBuilder();
		sb.append("\n=================== PRINTING ANAGRAMS ===================");
		for (Set<String> anagramSet : anagramSetList) {
			sb.append("\n").append(++i).append(". (").append(anagramSet.size()).append(")");
			for (String anagram : anagramSet) {
				sb.append("  ").append(anagram);
			}
		}
		sb.append("\n=========================================================");
		LOG.info("{}", sb);
	}

}
