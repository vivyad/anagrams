package anagrams;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Perform the process of checking the words that are anagrams.
 * <br/>
 * The methods within this utility class have resorted to the approach of using
 * prime-number to discover whether two or more words are anagrams.
 * <br/>
 * About Prime numbers: they are whole numbers that does not have any divisor except 1
 * and themself. List of prime numbers are 2, 3, 5, 7, 11, 13, 17, ... infinite.
 * 
 * @author Vivek
 * 
 * @created 07 Jul 2020
 * @modified 10 Aug 2020
 */
public class AnagramUtil {

	/**
	 * The unmodifiable list where the prime numbers are allocated for alphabet.
	 * Each prime number within the list represent a alphabets based on the position.
	 */
	public static final List<Integer> PRIME_NUMBER_FOR_ALPHABET = PrimeUtil.orderedPrimeNumberList((short) 26);

	/**
	 * Example input from the test
	 */
	public static final String INPUT = "\nacre\n" + "cat\n" + "tree\n" + "race\n" + "care\n" + "act\n" + "bee\n";


	private AnagramUtil() throws InvocationTargetException {
		// Utility classes, which are collections of static members, are not meant to be
		// instantiated.
		throw new InvocationTargetException(new IllegalAccessException("Utility class"));
	}

	/**
	 * Calculates the product of prime numbers associated with all the respective 
	 * characters of input word. If <i>1</i> is returned then the input text is not
	 * a valid word for generating the primer product.
	 * 
	 * @param input {@link String}
	 *             Input text.
	 * @return primeProduct {@link Long}
	 *             Product of all the prime number of the characters.
	 */
	public static long getPrimeProduct(final String input) {
		if (input == null || input.trim().length() < 2) {
			return 1;
		}
		final char[] charsInput2 = input.trim().toLowerCase().toCharArray();
		long primeProduct = 1;
		// The variable 'letter' is ASCII value of the character
		for (final char letter : charsInput2) {
			// Only English alphabets are in consideration here.
			// ASCII a->97, b->98, ... , z->122
			if (!Character.isAlphabetic(letter)) {
				return 1;
			}
			// Using ASCII value of alphabets to get the index of prime number 
			// within the list. Therefore [0]->a, [1]->b, .., [25]->z
			final int primeNo = AnagramUtil.PRIME_NUMBER_FOR_ALPHABET.get(letter - 97);
			primeProduct *= primeNo;
		}
		return primeProduct;
	}

	/**
	 * Processes the input text into a structure format of list that contains all
	 * the anagrams found in the text.
	 * <br/>
	 * 
	 * Assigns the input words to the {@link Map} with [key=prime-product] and
	 * [value=set-of-word]. 
	 * <br/>
	 * Since, the words 'eat' and 'ate' will have the same the prime-product, that
	 * is '1562', hence, the key-value format of the map data-structure is
	 * [1562->{"ate","eat"}], where prime product '1562' with the <code>long</code>
	 * data-type is the key, and set of words {"ate","eat"} is value.
	 * <br/>
	 * The order of the words is not guaranteed within the {@link Set}
	 * data-structure because it is implemented through the class {@link HashSet}.
	 * <br/>
	 * 
	 * Discovers the anagram from the given {@link Map} with [key=prime-product] and
	 * [value=set-of-word].
	 * <br/>
	 * The method performs inspection through the evaluation of the number of words
	 * associated to single key. If more than one word is associated to one prime
	 * product or key from the input {@link Map} then set of those words are
	 * classified as anagrams.
	 * <br/>
	 * After discovering the set(s) with anagrams, if there exist multiple sets then
	 * those sets are organized based on the length of the word. For example, consider
	 * the sets of anagram given below:
	 * <ul>
	 *   <li>length 5: ["aster","rates"]</li>
	 *   <li>length 3: ["not","ton"]</li>
	 *   <li>length 4: ["east","eats"]</li>
	 *   <li>length 3: ["eat","ate"]</li>
	 * </ul>
	 * The above set of words are ordered as follows:
	 * <ul>
	 *   <li>length 3: ["not","ton"]</li>
	 *   <li>length 3: ["eat","ate"]</li>
	 *   <li>length 4: ["east","eats"]</li>
	 *   <li>length 5: ["aster","rates"]</li>
	 * </ul>
	 * 
	 * @param input {@link String}
	 *             Text to be processed for checking.
	 * @return list {@link List}
	 *             Contains all the discovered {@link Set} of anagrams.
	 */
	public static List<Set<String>> getAnagramList(final String input) {
		// Validate Input
		if (input == null) {
			return Arrays.asList();
		}
		// Remove if there is 'line-break' at the beginning or the end of the input text
		final List<String> words = Stream.of(input.trim().split("\n")).map (s -> s).collect(Collectors.toList());
		final int len = words.size();
		if (len < 2) {
			return Arrays.asList();
		}

		// Process the input words and get map
		final TreeMap<Integer, List<Set<String>>> anagramSetTreeMap = AnagramUtil.processAnagrams(words);

		// If the map is empty, then return the empty list.
		if (anagramSetTreeMap.isEmpty()) {
			return Arrays.asList();
		}

		final List<Set<String>> anagramSetList = new ArrayList<>();
		// No special sorting required
		anagramSetTreeMap.values().forEach(value -> anagramSetList.addAll(value));
		return anagramSetList;
	}


	/**
	 * Equip the main approach with divide and conquer strategy.
	 * <br/>
	 * This function uses the same approach of mapping the prime-product with one
	 * difference, Length of the words is key of the outer map and the value is
	 * another map and inner map holds [key=prime-product] and [value=[set-of-word]]
	 * pair.
	 * <br/>
	 * The structure of the returned map looks like:
	 * {
	 * 	 // word-length
	 *   3: {
	 *      // prime-product
	 *      30: [
	 *   	    cab,
	 *   	    abc
	 *      ],
	 *      // prime-product
	 *      710: [
	 *   	    cat,
	 *   	    act
	 *      ]
	 *   },
	 * 	 // word length
	 *   4: {
	 *      // prime-product
	 *      1293017: [
	 *   	    more,
	 *   	    rome
	 *      ] 
	 *   }
	 * }
	 * 
	 * @param words
	 *             Words to be processed for checking.
	 * @return anagramSetMap {@link TreeMap}
	 *             Contains all the discovered {@link Set} of anagrams mapped to word-length.
	 */
	private static TreeMap<Integer, List<Set<String>>> processAnagrams(final List<String> words) {
		// *** Codes are placed considering Stack Memory in mind. Therefore the most
		// frequently used 'instances/objects' are placed near the 'for' loop to work
		// effortlessly due to the 'FIFO' approach adhered by the Stack.

		// Map to store only the anagrams
		final TreeMap<Integer, List<Set<String>>> anagramSetMap = new TreeMap<>();

		// To identify if the prime-product is flagged, i.e. having multiple words.
		final Set<Long> primeProducts = new HashSet<>();

		// Map that performs the whole business here.
		// Length of the word is [key] of the outer map and the value is another map.
		// Inner map holds [key=prime-product] and [value=[set-of-word]] pair.
		final Map<Integer, Map<Long, Set<String>>> wordSetLenMap = new HashMap<>();
		
		for (final String word : words) {
			// Word having length either 2 or greater, are considered.
			final long primeProduct = AnagramUtil.getPrimeProduct(word);
			if (primeProduct <= 1) {
				continue;
			}
			final int wordLen = word.length();

			Map<Long, Set<String>> wordSetMap = wordSetLenMap.get(wordLen);
			if (wordSetMap == null) {
				wordSetMap = new HashMap<>();
				wordSetLenMap.put(wordLen, wordSetMap);
				Set<String> anagramSet = new TreeSet<>();
				wordSetMap.put(primeProduct, anagramSet);
				anagramSet.add(word);
			}
			// Else condition means that the 'wordSetLenMap' already contains the word(s)
			// of the same length
			else {
				Set<String> anagramSet = wordSetMap.get(primeProduct);
				// Check if the set-of-word is mapped with the given prime product.
				if (anagramSet == null) {
					anagramSet = new TreeSet<>();
					wordSetMap.put(primeProduct, anagramSet);
				}
				// The 'if' condition not being executed signifies that the word is an anagram.
				else if (!primeProducts.contains(primeProduct)) {
					primeProducts.add(primeProduct);
					final List<Set<String>> setList = anagramSetMap.computeIfAbsent(wordLen, l -> new ArrayList<>());
					setList.add(anagramSet);
				}
				anagramSet.add(word);
			}
		}
		wordSetLenMap.clear();
		return anagramSetMap;
	}

}
