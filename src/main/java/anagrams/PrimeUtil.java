package anagrams;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.math3.primes.Primes;

/**
 * Utility class to work with the prime number.
 * 
 * @author Vivek
 * 
 * @created 06 Jul 2020
 * @modified 07 Jul 2020
 *
 */
public class PrimeUtil {

	private PrimeUtil() throws InvocationTargetException {
		// Utility classes, which are collections of static members, are not meant to be
		// instantiated.
		throw new InvocationTargetException(new IllegalAccessException("Utility class"));
	}

	/**
	 * Returns the list of prime number holding the size equivalent to the
	 * respective input count.
	 * <br/>
	 * Node: accepting <code>count</code> as <code>short</code> or 
	 * <code>smallint</code> value and not <code>int</code>, to prevent the <br/>
	 * "<i>java.lang.OutOfMemoryError</i>: Requested array size exceeds VM limit"
	 * when the output is printed on the console, on the machines with lower
	 * hardware configuration.
	 * 
	 * @param count {@link Short}
	 *             Prime numbers to be created.
	 * @return list {@link List}
	 *             Containing prime numbers in the ascending order.
	 */
	public static List<Integer> orderedPrimeNumberList(final short count) {
		if (count < 1) {
			return new ArrayList<>();
		}
		final List<Integer> list = new ArrayList<>(count);
		// Lets start with 1, so we get prime numbers after 1 i.e. 2, 3, 5 , ..., n
		int currentPrime = 1;
		for (int i = 0; i < count; i++) {
			currentPrime = Primes.nextPrime(currentPrime + 1);
			list.add(Integer.valueOf(currentPrime));
		}
		return Collections.unmodifiableList(list);
	}

}
