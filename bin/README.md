# Demo Works


# 1. Discover Anagrams


### About

This project is developed to discover the anagrams from the candidate (input) words passed as an input.

The content of [README.md](README.md) is sometimes not represented appropriately online, therefore, a PDF copy called [README.pdf](README.pdf) is available that perfectly represents the markdown content. 

   
#### Overview

1. [Project Details](#1.-project-details)

      1.1. [Programming Language and Java Development Kit (JDK)](#1.1.-programming-language-and-java-development-kit-(jdk))

      1.2. [Gradle](#1.2.-gradle)

      1.3. [External Libraries](#1.3.-external-libraries)

      1.4. [Folder Structure - src](#1.4.-folder-structure---src)

      1.5. [List Of Java Classes](#1.5.-list-of-java-classes)

      1.6 [Project Testing And Building](#1.6.-project-testing-and-building)

      1.7. [Logging](#1.7.-logging)

2. [Working With Prime Numbers And Words](#2.-working-with-prime-numbers-and-words)

      2.1. [PrimeUtil.java](#2.1.-primeutil.java)

      2.2. [AnagramUtil.java](#2.2-anagramutil.java)

      - [Prime Product (PP) of a Word](#2.2.1.-prime-product-(pp)-of-a-word)

      - [Method - AnagramUtil.getPrimeProduct](#2.2.2.-method---anagramutil.getprimeproduct)

3. [Process Words And Return List Of Anagrams](#3.-process-words-and-return-list-of-anagrams)

      3.1. [Input Validation](#3.1.-input-validation)

      3.2. [Mapping Of Prime Product With Word](#3.2.-mapping-of-prime-product-with-word)

      3.3. [Identify Anagrams](#3.3.-identify-anagrams)

      3.4. [Sort Anagrams](#3.4.-sort-anagrams)

      3.5. [Section Conclusion](#3.5.-section-conclusion)

4. [Divide And Conquer](#4.-divide-and-conquer)

5. [Output](#5.-output)

6. [Testing](#6.-testing)

7. [Java Doc](#7.-java-doc)




## 1. Project Details


### 1.1. Programming Language and Java Development Kit (JDK)

Java is the sole programming language used in this project for the feature implementation, therefore, JDK is necessary in order to execute this project. For executing the project efficiently the version of JDK must be 8 or higher.


### 1.2. Gradle

Gradle is responsible for loading project dependencies, program and integration testing, and creating a build. Gradle loads the dependencies mentioned in the [build.gradle](build.gradle) file.


### 1.3. External Libraries

The libraries provided by JDK are sufficient to perform all the tasks within the project, but for some specific tasks, it is recommended to use the appropriate external libraries for executing those specific tasks instead of developing our own logic. The benefits of using the implementations provided by the external libraries are:

* It saves time and effort required for development and then, carry out the testing of the developed functionalities.
* Support from library maintainers and contributors.
* Implementations developed by libraries are more robust due to their extensive usage in a vast number of projects.
* From *no* to *a very low* emphasis dedicated towards the maintenance.

The list of external libraries used within this project are:

1. [Commons Math: The Apache Commons Mathematics Library](http://commons.apache.org/proper/commons-math/) version [3.6.1](http://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/index.html)
     * Used for generating prime numbers.
     * Apache License
2. [Simple Logging Facade for Java (SLF4J)](http://www.slf4j.org/) version [1.7.30](https://javadoc.io/doc/org.slf4j/slf4j-api/1.7.30/index.html)
     * Abstraction for Logging Framework
     * MIT License
3. [SLF4J LOG4J-12 Binding](https://javadoc.io/doc/org.slf4j/slf4j-log4j12) version [1.7.30](https://javadoc.io/doc/org.slf4j/slf4j-log4j12/1.7.30/index.html)
     * Performs binding of SLF4J logging with underlying Log4j logging implementation.
     * MIT License
4. [Apache Log4j Core](https://logging.apache.org/log4j/2.x/log4j-core/) version [2.11.2](https://logging.apache.org/log4j/2.x/log4j-core/apidocs/index.html)
     * Utility for implementation of Log4j logging functionality.
     * Apache License
5. [Apache Extras for Apache log4j](https://logging.apache.org/log4j/extras/) version [1.2.17](https://javadoc.io/doc/log4j/apache-log4j-extras/1.2.17/index.html)
     * Equips the Log4j Utility with additional features like 'FileNamePattern'.
     * Apache License
6. [Apache Commons IO](https://commons.apache.org/proper/commons-io/download_io.cgi) version [2.6](https://commons.apache.org/proper/commons-io/javadocs/api-2.6/index.html)
     * Used for performing Input/Output operation upon the file resources.
     * Apache License
7. [JUnit](https://junit.org/junit4/) version [4.12](https://github.com/junit-team/junit4/blob/HEAD/doc/ReleaseNotes4.12.md)
     * Provides Testing framework with unit test cases and test suites.
     * Eclipse Public License - v 1.0


### 1.4. Folder Structure - src

* [src/main/java](src/main/java) - Contains java clasess
* [src/main/resources](src/main/resources) -Contains resources used for project configuration
* [src/test/java](src/test/java) - Contains test classes
* [src/test/resources](src/test/resources) - Contains resource used for the testing purpose

### 1.5. List Of Java Classes

* [PrimeUtil.java](src/main/java/anagrams/PrimeUtil.java) - Utility class with the method to work with the prime numbers.
* [AnagramUtil.java](src/main/java/anagrams/AnagramUtil.java) - Primary Utility class that includes useful methods to discover the anagrams.
* [AnagramApp.java](src/main/java/anagrams/AnagramApp.java) - Application class.

More documentation of the above classes can be found within the respective source classes.

### 1.6. Project Testing And Building

#### Gradle Test

Execute command `sh ./gradlew test` to test the project.

```linux-script
$ sh ./gradlew test
Starting a Gradle Daemon (subsequent builds will be faster)

BUILD SUCCESSFUL in 8s
5 actionable tasks: 3 executed, 2 up-to-date
```

##### Image 1.6. JUnit Test Result
![JUnit Test Result](projectdocs/images/test_result.png)


#### Gradle Build

Execute command `sh ./gradlew build` to build the project.

```linux-script
sh ./gradlew build

BUILD SUCCESSFUL in 0s
6 actionable tasks: 1 executed, 5 up-to-date
```

### 1.7. Logging

The project logs are created within the [logs](/logs) folder. Since this project uses Apache's Log4j logging utility the logging functionality is configured through [log4j.properties](src/main/resources/log4j.properties) file.



## 2. Working With Prime Numbers And Words

In order to work with the prime numbers, this project makes use of the classes [PrimeUtil.java](src/main/java/anagrams/PrimeUtil.java) and [AnagramUtil.java](src/main/java/anagrams/AnagramUtil.java).


### 2.1. PrimeUtil.java

[PrimeUtil](src/main/java/anagrams/PrimeUtil.java) is a utility class with only one static method i.e. `orderedPrimeNumberList(short count)` that accepts a small integer (short) ***'count'*** as an input parameter and returns a list  of type `java.util.List`, containing unique prime numbers. The length of the output list returned by the method `orderedPrimeNumberList` is equivalent to the ***count***, that is passed as an input parameter to the method.

Execution and output of method  `orderedPrimeNumberList(short count)` is shown in [Code Section 2.1](#code-section-2.1) and [Code Section 2.2](#code-section-2.2)

##### Code Section 2.1
```java
final List<Integer> list = PrimeUtil.orderedPrimeNumberList(5);
System.out.println(list);
final List<Integer> list2 = PrimeUtil.orderedPrimeNumberList(8);
System.out.println(list2);
```

Output:
##### Code Section 2.2

```
[ 2, 3, 5, 7, 11]
[ 2, 3, 5, 7, 11, 13, 17, 19]
```


### 2.2. AnagramUtil.java

[AnagramUtil](src/main/java/anagrams/AnagramUtil.java) is a utility class that contains a static variable called `PRIME_NUMBER_FOR_ALPHABET`, which is a list of type `java.util.List`, containing 26 unique prime numbers as displayed in the code section below.

##### Code Section 2.3

```
2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43,
47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101
```


#### 2.2.1. Prime Product (PP) of a Word

These 26 prime numbers are used for representing the 26 alphabets/letters from the English language. Representation of the 26 alphabets i.e. `a,b,c,...,x,y,z` using the prime numbers `2,3,5,...,89,97,101` from [Code Section 2.3](#code-section-2.3) is shown in the [Code Section 2.4](#code-section-2.4).

##### Code Section 2.4
```
a=2, b=3, c=5, d=7, e=11, f=13, g=17, h=19, i=23, j=29,
k=31, l=37, m=41, n=43, o=47, p=53, q=59, r=61, s=67,
t=71, u=73, v=79, w=83, x=89, y=97, z=101
```

Let's assume that we have a candidate word ***cab*** and we wish to allocate a *number* to the given word. One way to perform this activity is by finding all the prime numbers associated with all the letters with that word and multiply those prime numbers to form a number called prime product (PP).  
Using the information from [Code Section 2.4](#code-section-2.4), we can say that the PP of the word ***cab*** is `5 * 2 * 3 = 30` (where `c=5, a=2, b=3`). Please remember this technique because it is used in further sections, to discover the words that are anagrams.

#### 2.2.2. Method - AnagramUtil.getPrimeProduct

The task of creating the prime product of any given word is performed by the static method `getPrimeProduct(String input)` within [AnagramUtil](src/main/java/anagrams/AnagramUtil.java), that accepts an input *String* and returns a number with `long` *data-type* as an output, as displayed in [Code Section 2.5](#code-section-2.5) and [Code Section 2.6](#code-section-2.6)


##### Code Section 2.5
```java
final long primeProductCAB = AnagramUtil.getPrimeProduct("cab");
System.out.println(primeProductCAB);
```

Output:
##### Code Section 2.6
```
30
```



## 3. Process Words And Return List Of Anagrams

This section dispenses the knowledge about the solution, that discovers the anagrams from the input words. The UML Diagram in the [Image 3.1](#image-3.1.-uml) depicts the work flow of the general implementation.

##### Image 3.1. UML

![UML](projectdocs/images/process_words_and_return_list_of_anagrams​.jpg)


#### Input, processing and output
The static method `getAnagramList(String input)` with `public` access, is accountable for discovering the anagrams from the given input *String*. It takes the input (I), performs the processing task (P) and provides the output (O), the description of IPO is as follows:
* Input: Text of type `java.lang.String` that comprises of the multiple words.
* Processing: Discovers the anagrams (words) from the input *String*.
* Output: Returns a *list* of the type `java.util.List` containing the discovered anagrams.


The processing of input comprises of following 4 activities:  
   3.1. [Input Validation](#3.1.-input-validation)  
   3.2. [Mapping Of Prime Product With Word](#3.2.-mapping-of-prime-product-with-word)  
   3.3. [Identify Anagrams](#3.3.-identify-anagrams)  
   3.4. [Sort Anagrams](#3.4.-sort-anagrams) 



### 3.1. Input Validation

The input string supplied to the method `processAnagrams(String input)` has multiple words at each line and the method can identify the number of words by counting the total number of lines (with words) from the input *String*. If the input has either single word or no word at all, then a blank *List* is returned as an output. If there are multiple words present in the input *String* then the method divides the input into list of word and forwards the list.

##### Code Section 3.1
```java
// Validate Input
if (input == null) {
    return Arrays.asList();
}
// Remove if there is 'line-break' at the beginning or the end of the input text
final List<String> words = Stream.of(input.trim().split("\n")).map (s -> s).collect(Collectors.toList());
final int len = words.size();
if (len < 2) {
    return Arrays.asList();
}
................
// Process words
................
```


### 3.2. Mapping Of Prime Product With Word

To perform the mapping activity, the array of word passed after [Input Validation](#3.1.-Input-Validation) is used to run an iterative loop starting from the first position in the array and traversing untill the last position of the array; i.e. the loop count is equivalent to the number of words within the given array.

##### Code Section 3.2
```java
for (final String word : words) {
   final long primeProduct = AnagramUtil.getPrimeProduct(word);
   // Map prime product
}
```

#### Mapping all words of array to prime product

In each iteration a word from the array is picked up and it is passed as an input to the method `AnagramUtil.getPrimeProduct(word)` (See [2.2.2 Method - AnagramUtil.getPrimeProduct](#2.2.2.-method---anagramutil.getprimeproduct)), and in response the method `AnagramUtil.getPrimeProduct` generates prime product using the approach mentioned in [2.2.1 Prime Product (PP) of a Word](#2.2.1.-prime-product-(pp)-of-a-word), and returns the prime product for the given word as an output.

The resultant prime product generated for a given word is mapped to the respective word. To elaborate this approach lets consider the same example from  [2.2.1 Prime Product (PP) of a Word](#2.2.1.-Prime-Product-(PP)-of-a-Word), where the given word was ***cab*** and it's resultant prime product was ***30***, then the mapping looks exactly like in the following code section.

##### Code Section 3.3
```
{ 30 -> [cab] }
```

Similarly, if we have few more words like ***can*** and ***abc*** then the prime products of those words will be `5 * 2 * 43 = 430` and `2 * 3 * 5 = 30` respectively. Therefore, after processing the 3 words i.e. ***cab***, ***can*** and ***abc***, the mapping of word to their respective prime product would look exactly like in the following code section.

##### Code Section 3.4
```
{ 30 -> [cab, abc] , 430 -> [can] }
```


### 3.3. Identify Anagrams

The activity to identify the anagrams from the input words executes alongside to the [3.2. Mapping Of Prime Product With Word](#3.2.-mapping-of-prime-product-with-word) activity, within the same iteration cycle to save the computation cost and time, as shown in the code section below.

##### Code Section 3.5
```java
for (final String word : words) {
   // Map prime product
   // Identify anagrams
}
```

In this process, the condition: whether the multiple words are mapped to single prime product, is checked, and if any prime product is mapped with multiple words then those words are marked as anagrams.
From [Code Section 3.4](#code-section-3.4) the prime product ***30*** is mapped to the words ***cab*** and ***abc***, hence, they are considered anagrams.

### 3.4. Sort Anagrams

Suppose, the anagrams are as follows.

- length 5: ["aster","rates"]
- length 3: ["not","ton"]
- length 4: ["east","eats"]
- length 3: ["eat","ate"]

Then, the anagrams should be ordered in the following way.

* length 3: ["not","ton"]
* length 3: ["ate","eat"]
* length 4: ["east","eats"]
* length 5: ["aster","rates"]

 The task of sorting the anagrams is performed by `java.util.TreeMap` and `java.util.TreeSet` in the most efficient way.

### 3.5. Section Conclusion

The method `getAnagramList(String word)` from class [AnagramUtil]
(src/main/java/anagrams/AnagramUtil.java) takes care of [3.1.Input Validation]
(#3.1.-input-validation) process but it internally uses the method 
`processAnagrams(List<String> words)`, which implements the approaches exactly as 
demonstrated in the subsections [3.2 Mapping Of Prime Product With Word]
(#3.2.-mapping-of-prime-product-with-word) and [3.3 Identify Anagrams]
(#3.3.-identify-anagrams).


##### Code Section 3.7
```java
class AnagramUtil {

    // Primary Method
    public static List<Set<String>> getAnagramList(final String input) {
		// Validate Input
		if (input == null) {
			return Arrays.asList();
		}
		// Remove if there is 'line-break' at the beginning or the end of the input text
		final List<String> words = Stream.of(input.trim().split("\n")).map (s -> s).collect(Collectors.toList());
		final int len = words.size();
		if (len < 2) {
			return Arrays.asList();
		}

		// Process the input words and get map
		final TreeMap<Integer, List<Set<String>>> anagramSetTreeMap = AnagramUtil.processAnagrams(words);
        ....
        ....
    }

    ....
    ....

    //Internal Method
    private static TreeMap<Integer, List<Set<String>>> processAnagrams(final List<String> words)
       // PERFORMS
           // 3.2 Mapping Of Prime Product With Word
           // 3.3. Identify Anagrams
       // RETURNS Map
       return anagramListMap;
    }
}
```



## 4. Divide And Conquer

By now, the sub sections  [3.2 Mapping Of Prime Product With Word](#3.2.-mapping-of-prime-product-with-word) and [3.3 Identify Anagrams](#3.3.-identify-anagrams) must have made you familiar with the general idea. Now, it is time to take the chapter a level further and understand the 'divide and conquer' setup. The divide and conquer approach adds flavor (a division of words) to the previously mentioned approaches from the subsections [3.2 Mapping Of Prime Product With Word](#3.2.-mapping-of-prime-product-with-word) and [3.3 Identify Anagrams](#3.3.-identify-anagrams), let's see how.

If you are given a set of 1000 things to work with what would be the best approach to complete your task smoothly. The answer is: divide or organize them according to their category. Actually, we do this ourselves in our life daily like, keeping clothes in one section of the living space while books in the other section and shoes somewhere else. Similarly, structures from [Code Section 4.1](#code-section-4.1) and [Code Section 4.2](#code-section-4.2) represent what I just said and they will help you to grasp the understanding of the idea in hand, more technically. According to you, which structure from the code sections below is more easier to deal with? Well, I would say that both are easier to deal with, because there are only 3 prime products in question. Now, consider if there are 10000 prime products, or maybe even more, you would now agree that the structure from the [Code Section 4.2](#code-section-4.2) provides an easier way of handling the elements when there are lot items to work with.

##### Code Section 4.1

```
1.   {
2.     	// prime-product
3.     	30: [
4.     		cab,
5.     		abc
6.     	],
7.     	// prime-product
8.     	710: [
9.     		act,
10.     	cat
11.    	]
12.    	// prime-product
13.    	1293017: [
14.    		more,
15.    		rome
16.    	]
17.   }
```

##### Code Section 4.2
```
1.   {
2.      // word-length
3.      3: {
4.     	   // prime-product
5.     	   30: [
6.     		   cab,   	// length of word is 3
7.     		   abc   	// length of word is 3
8.     	   ],
9.     	   // prime-product
10.    	   710: [
11.     	   act,   	// length of word is 3
12.     	   cat   	// length of word is 3
13.        ]
14.      },
15.      // word length
16.     4: {
17.    	   // prime-product
18.    	   1293017: [
19.    		   more,   	// length of word is 4
20.    		   rome   	// length of word is 4
21.    	   ] 
22.     }
23.   }
```

The [Code Section 4.2](#code-section-4.2) contains a *map* which has an *inner map*. By now you must be familiar with the representation of the *inner map*, from the previous sections, if you have read them! If not then the inner map is a prime-product mapped to the words that have the same prime-product (which means that they anagrams). The inner map(s) are mapped with a number that is of type `integer`, thus, forming another map or an outer map. The number with which the inner maps are mapped with, is actually the length of words within the inner map; if you will notice [Code Section 4.2](#code-section-4.2) carefully, you will know what exactly I mean.

This approach may be futile while processing fewer words, but when it comes to processing a large number of words, this approach is quite significant due to its ability to decrease the search cost.

##### Code Section 4.3
```java
class AnagramUtil {

    // Primary Method
    public List<Set<String>> getAnagramList(final String input) {
       ...
		// Process the input words and get map
		TreeMap<Integer, List<Set<String>>> anagramSetTreeMap = AnagramUtil.processAnagrams(words);

		// If anagram count is 0, then return the empty list.
		if (anagramSetTreeMap.isEmpty()) {
			return Arrays.asList();
		}

		List<Set<String>> anagramSetList = new ArrayList<>();
		// No special sorting required
		for (List<Set<String>> anagramList : anagramSetTreeMap.values()) {
			anagramSetList.addAll(anagramList);
		}
		return anagramSetList;
    }

    ....

    //Internal Method
    private static TreeMap<Integer, List<Set<String>>> processAnagrams(final List<String> words) {
		// Implements Divide and Conquer Approach Here along with 
        //  - 3.2. Mapping Of Prime Product With Word
        //  - 3.3. Identify Anagram
       return anagramListMap;
    }
}
```

The internal (private) method `processAnagrams(List<String> words)` within [AnagramUtil](src/main/java/anagrams/AnagramUtil.java) class, implements this approach along with  [3.2 Mapping Of Prime Product With Word](#3.2.-mapping-of-prime-product-with-word) and [3.3 Identify Anagrams](#3.3.-identify-anagrams). As you can see in the [Code Section 4.3](#code-section-4.3), the method `getAnagramList` calls `processAnagrams` method to fetch the map of anagrams having structure depicted in [Code Section 4.2](#code-section-4.2). If the map is empty, then the method `getAnagramList` returns the empty list. If the map is not empty then the set of words from the map are copied to a list of type `java.util.List`. So that the structure from [Code Section 4.2](#code-section-4.2) becomes like structure in mentioned below in [Code Section 4.4](#code-section-4.4)

##### Code Section 4.4
```
[ cab, abc ]
[ act, cat ]
[ more, rome ]
```

#### Self Sorting

Since the elements within the map are already sorted according to the length of words by the `TreeMap`, we do not have to be concerned about sorting the anagrams according to the length of words at all.

Since the anagrams are collected in a `java.util.TreeSet`, therefore, the anagrams are alphabetically sorted by the `TreeSet`; so need to do anything in that direction as well.



## 5. Output

The file ***input.txt*** within the [src/test/resources](src/test/resources) folder contains the following content.

##### Code Section 5.1

```
cat
tree
race
care
acre
bee
```

Upon executing the ***unit test function*** `testMain()` from the Test Class [AnagramAppTest](src/test/java/anagrams/AnagramAppTest.java),  the file named ***input.txt*** is processed by method `getAnagramList(String input)`from the class  [AnagramUtil](src/main/java/anagrams/AnagramUtil.java) and it returns the list as an output containing the following anagrams. 

##### Code Section 5.2
```
act  cat
acre  care  race
```



## 6. Testing

The project has been tested throughly using the test cases delivering 100% coverage. The functionality  testing for processing words and giving valid anagrams as an output, is performed using 4 components:

1. Input String
     * Within the  [AnagramUtil](src/main/java/anagrams/AnagramUtil.java) class.
2. Small File
     * ***input.txt*** within the [src/test/resources](src/test/resources) folder
     * Contains 7 words
3. Big File
     * ***input_big.txt*** within the [src/test/resources](src/test/resources) folder
     * Contains 871 words
4. Huge File
     * ***input_biggest.txt*** within the [src/test/resources](src/test/resources) folder
     * Contains 33967 words



## 7. Java Doc

You may find the Java-Doc for this project in the [javadoc](javadoc) folder, containing [index.html](javadoc/index.html).

-------------------

[Navigate to top](#demo-works) 